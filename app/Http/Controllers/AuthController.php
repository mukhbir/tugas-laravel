<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg(){
        return view('register');
    }

    public function post(Request $request){
        $firstname = $request['fname'];
        $lastname = $request['lname'];
        return view('welcome', compact('firstname','lastname'));
    }
}
