<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Home</title>
</head>
<body>
<h1>Media Online</h1>
<h2>Sosial Media Developer</h2>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

<h3>Benefit Join di Media Online</h3>
    <ul>
        <li><p>Mendapatkan motivasi dari sesama para Developer</p></li>
        <li><p>Sharing knowledge</p></li>
        <li><p>Dibuat oleh calon web developer terbaik</p></li>
    </ul>  

<h3>Cara Bergabung ke Media Online</h3>
    <ol type="1">
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
     </ol>

</body>
</html>